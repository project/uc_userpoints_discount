Userpoints Discount
By: Ryan (rszrama)

Sponsored by: http://www.horseracegame.com

The userpoints discount makes it possible for customers to add items to their
shopping carts that are discounted based on the number of userpoints they have.
Discounts are enabled for individual products on their node edit forms, causing
a second submit button to be added to the product's add to cart form that adds
the product to the shopping cart with the discount.  The value of userpoints
are specified as a conversion rate against the dollar (or your store's currency)
in an administration form.

This module alters the cart view form so a customer can see their discounts
before proceeding to the checkout form.  During checkout, discounts are
represented and added to the orders as line items.  A customer's userpoints are
held while the item is in their shopping cart so they can't get more discounts
than they deserve!  These will expire if checkout is not completed, or they will
be permanently deducted from the user's account when checkout is completed.

This module should not be confused with a full Userpoints integration.  Its
scope is limited to the functionality described above.

For modifications of this module, please contact Ryan through the contact form
at http://www.ubercart.org/contact.  Thanks!
